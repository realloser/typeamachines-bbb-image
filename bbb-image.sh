#!/bin/bash
set -x

DIR=$PWD
hostname=series1-unconfigured

# Tests run for sanity

#  ${dist} is passed from the environment that called this script in the first place
check_defines () {
	if [ ! "${dist}" ] ; then
		echo "bbb-image.sh: Error: dist undefined"
		exit 1
	fi
	}

check_multistrap () {
	if [ ! "`which multistrap`" ] ; then
		echo "bbb-image.sh: Error: multistrap not installed"
		exit 1
	fi
	}

check_root () {
	if [ "$UID" -ne "0" ]
		then echo "bbb-image.sh Error: Please run as root"
		exit 1
	fi
	}

increment_version ()
{
  declare -a part=( ${1//\./ } )
  declare    new
  declare -i carry=1

  for (( CNTR=${#part[@]}-1; CNTR>=0; CNTR-=1 )); do
    len=${#part[CNTR]}
    new=$((part[CNTR]+carry))
    [ ${#new} -gt $len ] && carry=1 || carry=0
    [ $CNTR -gt 0 ] && part[CNTR]=${new: -len} || part[CNTR]=${new}
  done
  new="${part[*]}"
  echo -e "${new// /.}"
} 

check_root
check_defines
check_multistrap

# End of tests


# Build
build="`cat files/local-version`"
build="`increment_version $build`"
echo "$build" > files/local-version

# Timestamp
NIXDate="`date +%Y.%m.%d`"
NIXTime="$NIXDate-`date +%H.%M`"
ImageName="$dist-$NIXTime"
echo -e "\\n\\nBuild version $build - Build time $NIXTime\\n\\n"

# Issue Footer
source files/os-release.$dist
# Footer Example
#	Ubuntu 12.04.2 LTS \n \l
#
#	Firmware 1.8, build date 2014.05.11

Issue_Footer="$VERSION \\\\n \\\\l\\n\\nFirmware $build, build date $NIXDate"

#echo -e "$Issue_Footer"


# initialize image
mkdir -p "images/$ImageName" "source/$dist"
# truncate creates a file argument when it does not already exist, the -s switch allows one to set SIZE
# in bytes, this is 1750MB
truncate -s 1835008000 "images/$ImageName.image"
# mkfs.ext4 is a tool that simply creates an ext4 filesystem
mkfs.ext4 -F "images/$ImageName.image"
# mount the blank image as a loop device to the images directory
mount -o loop "images/$ImageName.image" "images/$ImageName"
# create archives and uboot directory tree on the image 
mkdir -p "images/$ImageName/var/cache/apt/archives/" "images/$ImageName/boot/uboot"
# Link source files to image so they aren't downloaded again
mount -o bind "source/$dist" "images/$ImageName/var/cache/apt/archives/"

# And use multistrap to build out the mounted image 
{ /usr/sbin/multistrap -f $dist.conf --dir "images/$ImageName" 2>&1; } 2>&1 | tee $ImageName.log

# copy custom files to image
cp -a files/resolv.conf files/fstab files/hosts "images/$ImageName/etc/"
cp -a files/firmware "images/$ImageName/lib/"
cp -a files/qemu-arm-static "images/$ImageName/usr/bin"
cp files/gadget-upstart_1.4cross_all.deb files/linux-3.13-rc0.deb files/second-stage.sh "images/$ImageName/tmp"
echo "$hostname" > "images/$ImageName/etc/hostname"


# bypass error with start executable when under qemu
mv "images/$ImageName/sbin/start" "images/$ImageName/sbin/start.bak"
echo "exit 0" > "images/$ImageName/sbin/start"
chmod 700 "images/$ImageName/sbin/start"

# configure parts of ubuntu
echo "America/Los_Angeles" > "images/$ImageName/etc/timezone"
echo 'ubuntu ALL=(ALL:ALL) ALL' >> "images/$ImageName/etc/sudoers"

# start chroot
mount -t proc proc "images/$ImageName/proc"
mount -o rbind /dev "images/$ImageName/dev"
mount -o rbind /sys "images/$ImageName/sys"
chroot "images/$ImageName" /bin/bash /tmp/second-stage.sh 2>&1 | tee -a $ImageName.log

# Cleanup
rm "images/$ImageName/tmp/second-stage.sh"
rm "images/$ImageName/etc/udev/rules.d/70-persistent-net.rules"
rm "images/$ImageName/lib/udev/rules.d/75-persistent-net-generator.rules"

# Post config setup

# Issue and MOTD timestamps and build info
cp "files/99-footer" "images/$ImageName/etc/update-motd.d/"
cp "files/issue" "files/motd.tail" "images/$ImageName/etc/."
cp "files/os-release.$dist" "images/$ImageName/etc/os-release"
echo "BUILD_ID=\"$build\"" >> "images/$ImageName/etc/os-release"
echo -e "$Issue_Footer" |tail -n1 >> images/$ImageName/etc/motd.tail
echo -e "$Issue_Footer" > images/$ImageName/etc/issue.net
echo -e "$Issue_Footer" >> images/$ImageName/etc/issue
cp "images/$ImageName/etc/ssh/sshd_config" "images/$ImageName/etc/ssh/sshd_config.bak"
sed s/PrintMotd No/PrintMotd Yes/g "images/$ImageName/etc/ssh/sshd_config.bak" > "images/$ImageName/etc/ssh/sshd_config"
rm "images/$ImageName/etc/ssh/sshd_config.bak"

# bug fixes
sed s/tty1/ttyO0/g "images/$ImageName/etc/init/tty1.conf" | sed s/38400/115200/g > "images/$ImageName/etc/init/ttyO0.conf"
cp "files/forgetsafe.conf" "images/$ImageName/etc/init/"
echo "manual" > "images/$ImageName/etc/init/failsafe.override"
cp "files/70-persistent-net.rules" "images/$ImageName/etc/udev/rules.d/70-persistent-net.rules"

# make sure that changes are actuall written to disk
sync
umount -fl "images/$ImageName/var/cache/apt/archives/" "images/$ImageName/*"
umount -fl "images/$ImageName/"
sync
#bzip2 -zk "images/$ImageName.image"
