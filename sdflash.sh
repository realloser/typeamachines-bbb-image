#! /bin/bash


if [ "$UID" -ne 0 ]
  then echo "Flash Error: Please run as root"
  exit
fi

if [! -b /etc/mmcblk0p2 ]
  then echo "Flash Error: Something is up"
  exit
fi


imageloc=.


clear

echo Flashing SD - DO NOT REMOVE!


dest_device=/dev/mmcblk0 source_image_part1=$imageloc/bbb.01.07.14-part1 source_image_part2=$imageloc/bbb.03.20.14-part2 bash $imageloc/flash.sh

#clear

echo You are free to go now.
