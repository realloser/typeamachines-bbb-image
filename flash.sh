#!/bin/bash


DIR=$PWD
check_defines () {
        if [ ! "${dest_device}" ] ; then
                echo "flash.sh: Error: dest_device undefined"
		echo ""
		echo "Usage : dest_device=/dev/mmcblk0 source_image_part1=bbb-boot-part source_image_part2=precise-1394660128.image sh flash.sh"
                exit 1
        fi

        if [ ! "${source_image_part1}" ] ; then
                echo "flash.sh: Error: source_image_part1 undefined"
		echo ""
		echo "Usage : dest_device=/dev/mmcblk0 source_image_part1=bbb-boot-part source_image_part2=precise-1394660128.image sh flash.sh"
                exit 1
        fi

        if [ ! "${source_image_part2}" ] ; then
                echo "flash.sh: Error: source_image_part2 undefined"
		echo ""
		echo "Usage : dest_device=/dev/mmcblk0 source_image_part1=bbb-boot-part source_image_part2=precise-1394660128.image sh flash.sh"
                exit 1
        fi
        
	if [ ! "${force}" ] ; then
		if [ "0`sfdisk -s ${dest_device} 2> /dev/null`0" -gt "377619200"  ] ; then

	                echo "flash.sh: Error: Device is large and \"force\" is undefined"
			echo ""
			echo "Usage : dest_device=/dev/mmcblk0 source_image_part1=bbb-boot-part source_image_part2=precise-1394660128.image sh flash.sh"
                exit 1
		fi
        fi
        }

check_images () {

	file "${source_image_part1}" | grep -q x86 &> /dev/null
	if [ "$?" != "0" ] ; then 
		echo "FS Impropper : ${source_image_part1} is not a fat partition or is not bootable"
		exit 1
	fi
	file "${source_image_part2}" | grep -q ext4 &> /dev/null
	if [ "$?" != "0" ] ; then 
		echo "FS Impropper : ${source_image_part2} is not an ext4 partition"
		exit 1
	fi

}

clean_mount () {
	sync
	umount -fl /dev/mapper/* "$dest_device" "$dest_device"* &> /dev/null
case `file -b "$dest_device"` in
	
	*block* ) 
		Part1="$dest_device"p1
		Part2="$dest_device"p2
		Part3="$dest_device"p3

	;;
	data ) 
		kpartx -d "$dest_device" &> /dev/null
		sync
	;;
	*x86* )
		kpartx -d "$dest_device" &> /dev/null
		sync
		declare -a Parts=(`kpartx -av "$dest_device" | cut -d" " -f3`)

		b=1
		for i in "${Parts[@]}"
		do
			
			export Part$(( b++ ))=/dev/mapper/"$i"
		done
	;;
	*cannot* ) echo no loop

		if [ ! $"size" ] ; then
			size="7761920"
		fi
		touch "$dest_device"
		kpartx -d "$dest_device" &> /dev/null
		truncate -s "$size" "$dest_device"
		sync

	;;
	* ) echo "Unrecognized device or image.  If an image file exists, please delete it and try again."
		exit 1
	;;
esac
}

#set -x 
echo "==============================================================================="
echo -e "\\nFlashing $dest_device\\nPlease wait for prompt before removing SD card\\n"


check_defines
check_images
clean_mount  

sfdisk -q -f $dest_device << "EOF" &> /dev/null
unit: sectors

/dev/mmcblk0p1 : start=     2048, size=     2048, Id= 6
/dev/mmcblk0p2 : start=     4096, size=  3747840, Id=83
/dev/mmcblk0p3 : start=  3751936, size= , Id=83
/dev/mmcblk0p4 : start=        0, size=        0, Id= 0
EOF

clean_mount

partprobe $dest_device

clean_mount 
echo -e "Formatting $Part3"
mkfs.ext4 -qF "$Part3" -L "Prints" -E root_owner=1000:1000

clean_mount 

dd if="$source_image_part1" bs=4M status=none | pv -s `ls -la "$source_image_part1" | cut -d" " -f5` | dd of="$Part1" bs=4M status=none

clean_mount 

echo -e "Writing complete\\n\\nWriting $Part2"
dd if="$source_image_part2" bs=4M status=none | pv -s `ls -la "$source_image_part2" | cut -d" " -f5` | dd of="$Part2" bs=4M status=none

clean_mount 

echo "Image process complete"
